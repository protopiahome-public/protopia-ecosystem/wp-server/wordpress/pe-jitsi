<?php
error_reporting(E_ALL);
/*
Plugin Name: ProtopiaEcosystem Jitsi 
Plugin URI: http://genagl.ru/?p=652
Description: Support for Jitsi Meets by Appolo GraphQL clients
Version: 0.0.121
Author: Genagl
Author URI: http://genagl.ru/author
Contributors: genag1@list
License: GPL2
Text Domain:   pejitsi
Domain Path:   /lang/
*/
/*  Copyright 2018-2020  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 
 

//библиотека переводов
function init_textdomain_pejitsi() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("pejitsi", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_pejitsi');
require_once ABSPATH . 'wp-admin/includes/plugin.php';

/*
if( !is_plugin_active( "bio/bo.plugin.php" ) )
{
	return;
}
*/

define('PEJITSI_URLPATH', WP_PLUGIN_URL.'/pe-jitsi/');
define('PEJITSI_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('PEJITSI', 'pejitsi');
define('PE_TRANSLATION_TYPE', 'pe_translation');
define('PE_ROOM_TYPE', 'pe_room');

require_once( PEJITSI_REAL_PATH . 'class/PE_Jitsi.class.php' );

if (function_exists('register_activation_hook'))
	register_activation_hook( __FILE__, array( "PE_Jitsi", 'activate' ) );
if (function_exists('register_deactivation_hook'))
	register_deactivation_hook(__FILE__, array("PE_Jitsi", 'deactivate'));

add_action("init", "init_PE_Jitsi", 3);
add_action( 'plugins_loaded', 'PE_Jitsi_action_function_name', 12 );
function init_PE_Jitsi()
{
	PE_Jitsi::get_instance();
	PE_Translation::init();
	PE_Room::init();
	PE_Ajax::get_instance();
}
function PE_Jitsi_action_function_name() 
{
	if(!class_exists("SMC_Post") )
	{		
		require_once(PEJITSI_REAL_PATH.'lib/SMC_Post.php');
		require_once(PEJITSI_REAL_PATH.'lib/SMC_Taxonomy.class.php');
	}
	require_once(PEJITSI_REAL_PATH.'class/PE_Translation.class.php');
	require_once(PEJITSI_REAL_PATH.'class/PE_Room.class.php');
	require_once(PEJITSI_REAL_PATH.'class/PE_Ajax.class.php');
}