<div align="center">
<h1>Oraculi</h1>
<img src="pe_oraculi.jpg">

</div>

---


Oraculi is LMS powered by Protopia Ecosystem.


## Requirements

## Installation and launch

## Contributing
You can help by working on [opened issues](https://gitlab.com/protopiahome-public/it-public/oraculi/oraculi-react/-/issues), fixing bugs, creating new features, improving documentation.

Before contributing, please read [CONTRIBUTING.md](CONTRIBUTING.md) first.


## License
Oraculi React is released under an Apache 2.0 license. See [LICENSE](LICENSE) for the full licensing conditions.
