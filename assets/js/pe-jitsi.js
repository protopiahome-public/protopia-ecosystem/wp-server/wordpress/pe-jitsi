var setmsg ;

function __(text)
{
	return text;
}	
function detach( d )
{
	(function($){
		$(d).detach();
	})(jQuery)
}

jQuery(document).ready(function($)
{	  
	$(".pe_jitsi_options").live({change : function(evt)
	{
		console.log( $(this).attr("name"), $(this).val() );
		pe_jitsi_send(['pe_jitsi_options', $(this).attr("name"), $(this).val() ]);	
	}})
	$("#add_jitsi_rooms").live({click: function(evt)
	{
		var post_type = $("[name=added_room_type]").val();
		if( post_type == -1 )
		{
			alert("Select SMC Type");
			return;
		}
		pe_jitsi_send(["added_room_type", post_type]);
	}});
	
	document.documentElement.addEventListener("my_image_upload", function(evt) 
	{
		if( evt.detail.prefix == "default_img" )
		{
			pe_jitsi_send(['bio_options', "default_img", evt.detail.id  ]);
		}
		if( evt.detail.prefix == "404" )
		{
			pe_jitsi_send(['bio_options', "404", evt.detail.id  ]);
		}
	})
})


function set_message(text)
{
	//jQuery(".msg").detach();
	clearTimeout(setmsg);
	var msg = jQuery("<div class='msg' id='msg_"+msgn+"'><div class='close'><span aria-hidden='true'>&times;</span></div>" + text + "</div>").appendTo("#msgc").hide().fadeIn("slow");
	setTimeout( 
		function(msg) 
		{
			msg.fadeOut(700, msg.detach());
		}, 
		6000, 
		$("#msg_" + msgn)
	);
	msgn++;
}
var msgn=0;


function pe_jitsi_send( params, type )
{
	console.log(params, type);
	jQuery("body")
		.addClass("blocked")
			.append(jQuery("<div class='bio_wait' id='wait'><i class='fas fa-sync-alt fa-spin'></i></div>")
				.fadeIn("slow"));
	body_wait = setTimeout(function()
	{
		jQuery("body").removeClass("blocked").remove("#wait");
		jQuery("#wait").detach();
	}, 7000);
	jQuery.post	(
		pejitsiajax.url,
		{
			action	: 'pejitsiajax',
			nonce	: pejitsiajax.nonce,
			params	: params
		},
		function( response ) 
		{
			console.log(response);
			try
			{
				var dat = JSON.parse(response);
			}
			catch (e)
			{
				return;
			}
			//alert(dat);
			var command	= dat[0];
			var datas	= dat[1];
			//console.log(command);
			switch(command)
			{
				case "test":
					break;
				default:
					var customEvent = new CustomEvent("_pe_send_", {bubbles : true, cancelable : true, detail : dat})
					document.documentElement.dispatchEvent(customEvent);					
					break;
			}			
			if(datas['exec'] && datas['exec'] != "")
			{
				window[datas['exec']](datas['args']);
			}
			if(datas['a_alert'])
			{
				alert(datas['a_alert']);
			}
			if(datas.msg)
			{
				set_message(datas.msg)
			}
			jQuery("body").removeClass("blocked").remove("#wait");
			jQuery("#wait").detach();
		}		
	);
}  