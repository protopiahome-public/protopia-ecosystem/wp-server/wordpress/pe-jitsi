<?php
if(!class_exists("ExceptionNotLogged"))
{
	class ExceptionNotLogged extends Exception
	{
		public function Send()
		{
			// отсылаем содержимое ошибки на email админа
		}
	}
}
if(!class_exists("ExceptionNotAdmin"))
{
	class ExceptionNotAdmin extends Exception
	{
		public function Send()
		{
			// отсылаем содержимое ошибки на email админа
		}
	}
}
	
class PE_Ajax
{
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		add_action('wp_ajax_nopriv_pejitsiajax',		array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_pejitsiajax',				array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_pejitsiajax-admin', 		array(__CLASS__, 'ajax_submit'));		
	}
	static function ajax_submit()
	{
		try
		{
			static::pejitsiajax_submit();
		}
		catch(ExceptionNotLogged $ew)
		{
			$d	=  array(	
				-1,
				array(
						'a_alert'	=> __("Only logged users may do this!", BIO)
					  )
			);
			$d_obj	= json_encode(apply_filters("bio_ajax_data_not_owner", $d, $params));
			print $d_obj;
			exit;
		}
		catch(ExceptionNotAdmin $ew)
		{
			$d	=  array(	
				-2,
				array(
						'a_alert'	=> __("Only Administrator may do this!", BIO)
					  )
			);
			$d_obj	= json_encode(apply_filters("bio_ajax_data_not_admin", $d, $params));
			print $d_obj;
			exit;
		}
		catch(Error $ex)
		{
			$d = [	
				"Error",
				array(
					'msg'	=> $ex->getMessage (),
					'log'	=> $ex->getTrace ()
				  )
			];
			$d_obj		= json_encode( $d );				
			wp_die( $d_obj );
		}
		wp_die();
	}
	static function pejitsiajax_submit()
	{
		global $wpdb;
		$nonce = $_POST['nonce'];
		if ( !wp_verify_nonce( $nonce, 'pejitsiajax-nonce' ) ) die ( $_POST['params'][0] );
		
		$params	= $_POST['params'];	
		$d		= array( $_POST['params'][0], array() );				
		switch($params[0])
		{				
			case "test":	
				$d = array(	
					$params[0],
					array( 
						"text"		=> 'testing',
					)
				);
				break;			
			case "pe_jitsi_options":	
				$name = $params[1];
				$val  = $params[2];
				PE_Jitsi::$options[$name] = $val;
				update_option(PEJITSI, PE_Jitsi::$options);
				 
				$d = array(	
					$params[0],
					array( 
						"text"		=> 'pe_jitsi_options',
						$name		=> $val
					)
				);
				break;		
			case "added_room_type":	 
				$type = $params[1];
				$all = $type::get_all();
				$execs = [];
				foreach($all as $elem)
				{
					$ins  = $type::get_instance( $elem );
					$jitsi = $ins->get_meta("jitsi");
					$jitsi_password = $ins->get_meta("jitsi_password");
					if(!$jitsi)
					{
						$_id =  strtolower(static::jitsy_translit($ins->get("post_title"). MD5(time() + rand()) ) );
						$ins->update_meta("jitsi", $_id);
						$execs[] = $_id;
					}
					if(!$jitsi_password)
					{ 
						$ins->update_meta( "jitsi_password", MD5(time()+ rand()) );
					}
				}
				$d = array(	
					$params[0],
					array(  
						"type"		=> $type,
						"all"		=> count($all),
						"updates"	=> $execs
					)
				);
				break;
			case "bio_exec_ajax":
			{
				if(!current_user_can("manage_options"))
						throw new ExceptionNotAdmin();
				$d = apply_filters("", [], $params);
				break;
			}		
			default:
				$d = apply_filters("pe_ajax_submit", $d, $params);
				break;
		}
		$d_obj		= json_encode(apply_filters("pe_ajax_data", $d, $params));				
		print $d_obj;
		wp_die();
	}
	
	static function jitsy_translit($srt) 
	{
		$jitsy_rus = array(
		   "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"","є"=>"ye","ѓ"=>"g",
		   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
		   "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
		   "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
		   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
		   "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
		   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
		   "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
		   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
		   "е"=>"e","ё"=>"yo","ж"=>"zh",
		   "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
		   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
		   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
		   "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		   "«"=>"","»"=>"","…"=>"", "!"=>"", "?"=>"", ","=>"", "."=>"",
		   ":"=>"", ";"=>"", "`"=>"", "~"=>"", "'"=>"", '"'=>"", '{'=>"", 
		   '}'=>"", '['=>"",  ']'=>"",  '-'=>"",  '@'=>"",   '#'=>"",  '$'=>"", '%'=>"", 
		   '^'=>"", '&'=>"",  '*'=>"",  '+'=>"",  '='=>"",  '_'=>"",  ' '=>"",  '('=>"",  ')'=>""
		 );
		return strtr($srt, $jitsy_rus);
	}
	
}



