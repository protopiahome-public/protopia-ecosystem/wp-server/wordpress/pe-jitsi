<?php

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class PE_Jitsi
{
	static function activate()
	{		
		$options = get_option( PEJITSI );
		global $wpdb;
		
	}
	static function deactivate()
	{
		
	}
	
	static $options;	
	static $instance;
	
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	
	function __construct()
	{		
		static::$options = get_option( PEJITSI );
		add_action( 'admin_menu',					[ __CLASS__, 'admin_page_handler'], 9);
		add_filter( "smc_add_post_types",	 		[ __CLASS__, "init_obj"], 20);
		add_action( 'admin_enqueue_scripts', 		[__CLASS__, 'add_admin_js_script'] );
		
		// Add fields for BIO options gq-command
		// add_filter( "bio_gq_options", 				[ __CLASS__, "gq_options"], 50);
		add_filter( "bio_public_gq_options", 		[ __CLASS__, "public_gq_options"], 50);
		add_filter( "bio_gq_options_input", 		[ __CLASS__, "gq_options_input"], 50);
		add_filter( "bio_get_gq_options", 			[ __CLASS__, "get_gq_options"], 50);
		add_filter( "bio_get_public_gq_options", 	[ __CLASS__, "bio_get_public_gq_options"], 50);
		add_action( "bio_change_gq_options", 		[ __CLASS__, "change_gq_options"], 50);
	}
	
	
	static function add_admin_js_script()
	{	
		
		//css
		wp_register_style("bootstrap", PEJITSI_URLPATH . 'assets/css/bootstrap.min.css', array());
		wp_enqueue_style( "bootstrap");
		wp_register_style("formhelpers", PEJITSI_URLPATH . 'assets/css/bootstrap-formhelpers.min.css', array());
		wp_enqueue_style( "formhelpers");		
		wp_register_style("periodpicker", PEJITSI_URLPATH . 'assets/css/jquery.periodpicker.min.css', array());
		wp_enqueue_style( "periodpicker");
		wp_register_style("fontawesome", 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', []);
		wp_enqueue_style( "fontawesome" );
		
		wp_register_style(PEJITSI, PEJITSI_URLPATH . 'assets/css/bio.css', array());
		wp_enqueue_style( PEJITSI);
					
		wp_register_script(PEJITSI, plugins_url( '../assets/js/pe-jitsi.js', __FILE__ ), array());
		wp_enqueue_script(PEJITSI); 
		
		// load media library scripts
		wp_enqueue_media();
		//ajax
		wp_localize_script( 
			'jquery', 
			'pejitsiajax', 
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('pejitsiajax-nonce')
			)
		);	
	}
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__('PE Jitsi', PEJITSI), 
			__('PE Jitsi', PEJITSI),
			'manage_options', 
			'pe_jitsi_page', 
			array(__CLASS__, 'get_admin'), 
			" ", // icon url  
			'2'
		);		
	}
	static function get_admin()
	{
		require_once PEJITSI_REAL_PATH . "tpl/admin.php" ;
	}
	static function init_obj($init_object)
	{
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= ['type' => 'PE_Translation'];		
		$p['start_date']	= ['type' => "date", 	"name" => __("Start date", PEJITSI)];
		$p['end_date']		= ['type' => "date", 	"name" => __("End date", PEJITSI)];
		$p['external_id']	= ['type' => "string", 	"name" => __("Main Room ID", PEJITSI)];
		$p['external_title']= ['type' => "string", 	"name" => __("Main Room Title", PEJITSI)];
		$p['is_locked'] 	= ['type' => "boolean", "name" => __("Is locked by password?", PEJITSI)];
		$p['is_permanent'] 	= ['type' => "boolean", "name" => __("Is premanent?", PEJITSI)];
		/*
		$p["geo"]			= [
			'type' 		=> "array", 
			'class'		=> "number", 
			"name" 		=> __("GEO position", PEJITSI)
		];
		*/
		$p[PE_ROOM_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "PE_Room", 
			"object" 	=> PE_ROOM_TYPE, 
			"hidden"	=> 1,
			"name" 		=> __("Rooms", PEJITSI)
		];
		$init_object[PE_TRANSLATION_TYPE]	= $p;
		
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= ['type' => 'PE_Room'];	
		$p['is_locked'] 	= ['type' => "boolean", "name" => __("Is locked by password?", PEJITSI)];
		$p['password']		= ['type' => "string", 	"name" => __("Password", PEJITSI)];
		$p['external_id']	= ['type' => "string", 	"name" => __("Jitsi ID", PEJITSI)];
		$p[PE_TRANSLATION_TYPE] = [
			'type' => "post", 
			'smc_post'=> "PE_Translation", 
			"object" => PE_TRANSLATION_TYPE, 
			"name" => __("Translation", PEJITSI)
		];
		$init_object[PE_ROOM_TYPE]	= $p;
		
		
		return $init_object;
	}
	
	
	static function gq_options( $matrix )
	{	
		$matrix["jitsi_server_url"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'jitsi_server_url', PEJITSI ) 
		];
		return $matrix;
	}
	
	static function public_gq_options( $matrix )
	{	
		$matrix["jitsi_server_url"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'jitsi_server_url', PEJITSI ) 
		];
		return $matrix;
	}
	static function gq_options_input( $matrix )
	{		
		$matrix["jitsi_server_url"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'jitsi_server_url', PEJITSI ) 
		];
		return $matrix;
	}
	static function get_gq_options( $matrix )
	{
		$matrix["jitsi_server_url"]	= static::$options['jitsi_server_url'];
		return $matrix;
	}
	static function bio_get_public_gq_options( $matrix )
	{
		$matrix["jitsi_server_url"]	= static::$options['jitsi_server_url'];
		return $matrix;
	}
	static function change_gq_options ($argsInput)
	{
		if ( isset($argsInput["jitsi_server_url"]))
		{
			static::$options["jitsi_server_url"] = $argsInput["jitsi_server_url"];
		}
		update_option(PEJITSI, static::$options);
	}
}