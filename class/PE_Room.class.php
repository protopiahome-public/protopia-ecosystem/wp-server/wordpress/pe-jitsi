<?php 

class PE_Room extends SMC_Post
{
	static function get_type()
	{
		return PE_ROOM_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 13);	
		add_action('smc_post_matrix', 			[ __CLASS__, 'smc_post_matrix' ], 11, 2);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Room", PEJITSI), // Основное название типа записи
			'singular_name'      => __("Room", PEJITSI), // отдельное название записи типа Book
			'add_new'            => __("add Room", PEJITSI), 
			'all_items' 		 => __('Rooms', PEJITSI),
			'add_new_item'       => __("add Room", PEJITSI), 
			'edit_item'          => __("edit Room", PEJITSI), 
			'new_item'           => __("add Room", PEJITSI), 
			'view_item'          => __("see Room", PEJITSI), 
			'search_items'       => __("search Room", PEJITSI), 
			'not_found'          => __("no Rooms", PEJITSI), 
			'not_found_in_trash' => __("no Rooms in trash", PEJITSI), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Rooms", PEJITSI), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_jitsi_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title','editor','author','thumbnail','excerpt','comments' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	static function smc_post_matrix ( $arg, $p )
	{
		return $arg;
	}
}