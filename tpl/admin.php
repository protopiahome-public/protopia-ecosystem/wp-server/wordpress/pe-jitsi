<?php
	require_once(MCOURSES_REAL_PATH."tpl/input_file_form.php");
 
	require_once(_REAL_PATH."class/SMC_Object_type.php");
	$html = "<style>
			.nav.nav-tabs 
			{
				padding: 0;
			}
			select
			{
				max-width: 100%!important;
			}
			.list-group-item:first-child 
			{
				border-top-left-radius: 0;
				border-top-right-radius: 0;
				border-top-color: transparent;
			}
			.pe-jitsi-logo 
			{
				width: 70px;
				height: 70px;
				background-image: url(" . PEJITSI_URLPATH . "assets/img/003-blogger.png);
				background-size: cover;
				background-repeat: no-repeat;
				margin: 4px!important;
			}
		</style>
		<section>
			<div class='container'>
				<div class='row d-flex align-items-center'>
					<div class=' '>
						<div class='pe-jitsi-logo'></div>
					</div>
					<div class=' ml-3'>
						<div class='display-4'> " . 
							__("Protopia Ecosystem Jitsi Settings", PEJITSI) . 
						"</div>
					</div>
				</div>
				<div class='spacer-10'></div>
				<nav class=''>
				  <div class='nav nav-tabs' id='nav-tab' role='tablist'>
					<a class='nav-item nav-link active' id='nav-home-tab' data-toggle='tab' href='#nav-home' role='tab' aria-controls='nav-home' aria-selected='true'>".
						__("Main Settings", PEJITSI).
					"</a>
					<!--a class='nav-item nav-link' id='nav-settings-tab' data-toggle='tab' href='#nav-settings' role='tab' aria-controls='nav-settings' aria-selected='false'>".
						__("Main menu settings", PEJITSI).
					"</a-->
					<a class='nav-item nav-link' id='nav-utilities-tab' data-toggle='tab' href='#nav-utilities' role='tab' aria-controls='nav-utilities' aria-selected='false'>".
						__("Utilities", PEJITSI).
					"</a>
				  </div>
				</nav>
				
				<div class='tab-content' id='nav-tabContent'>
				  <div class='tab-pane fade show active' id='nav-home' role='tabpanel' aria-labelledby='nav-home-tab'>				  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='row'>
								<div class='col-md-4 col-sm-12 text-right p-2'>".
									__( "Jitsi server URL", PEJITSI ).
								"</div>
								<div class='col-md-8 col-sm-12'>
									<input 
										class='form-control pe_jitsi_options' 
										name='jitsi_server_url' 
										value='" .static::$options['jitsi_server_url']. "' 
									/>
								</div>
							</div>
						</li>
					</ul>				  
				  </div>
				  <div class='tab-pane fade' id='nav-settings' role='tabpanel' aria-labelledby='nav-settings-tab'>				  
					<ul class='list-group'>
						menu
					</ul>
					<button type='button' class='btn btn-primary save_menu_setting'>" . 
						__("Save all changes", PEJITSI) . 
					"</button>
				</div>
				<div class='tab-pane fade' id='nav-utilities' role='tabpanel' aria-labelledby='nav-utilities-tab'>			  
					<ul class='list-group'>
						<li class='list-group-item '>
							<div class='m-4'>
								<div class='raw'>
									<div class='col-md-12 col-sm-12 mb-3 lead'>".
										__("Add Talk Rooms to all elemets that empty by type", PEJITSI).
									"</div>
									<div class='col-md-4 col-sm-12'>".
										__("Choose SMC type", PEJITSI).
									"</div>
									<div class='col-md-8 col-sm-12 mb-2'>".
										SMC_Object_Type::dropdown(
											[
												"class" 	=> "form-control ",
												"selected"	=> static::$options["added_room_type"],
												"name"		=> "added_room_type"
											]
										).
									"	<div class='my-3'>
											<div class='btn btn-danger btn-lg' id='add_jitsi_rooms'>".
												__("Add rooms if not exists.", PEJITSI).
											".</div>
										</div>
									</div>					
								</div>
							</dv>
						</li>
					</ul>
				</div>
				
				<div class='spacer-10'></div>
				 <span id='is_save' style='vertical-align:top; display:inline-block; opacity:0;'>
					<svg aria-hidden='true' viewBox='0 0 512 512' width='40' height='40' xmlns='http://www.w3.org/2000/svg'>
						<path fill='green' d='M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z'></path>
					</svg>
				</span>			
				
				
				
			</div>
		</section>";
		echo $html;