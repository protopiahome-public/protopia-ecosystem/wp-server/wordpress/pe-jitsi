<?php
if(!function_exists("get_input_file_form"))
{
	function get_input_file_form()
	{
		return '<div class="container"> <br />
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="input-group image-preview">
								<input placeholder="" type="text" class="form-control image-preview-filename" disabled="disabled">
								<!-- do not give a name === do is not send on POST/GET --> 
								<span class="input-group-btn"> 
								<!-- image-preview-clear button -->
								<button type="button" class="btn btn-default image-preview-clear" style="display:none;"> <span class="glyphicon glyphicon-remove"></span> Clear </button>
								<!-- image-preview-input -->
								<div class="btn btn-default image-preview-input"> <span class="glyphicon glyphicon-folder-open"></span> <span class="image-preview-input-title">Browse</span>
									<input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
									<!-- rename it --> 
								</div>
								<button type="button" class="btn btn-labeled btn-primary"> <span class="btn-label"><i class="glyphicon glyphicon-upload"></i> </span>Upload</button>
								</span> </div>
							<!-- /input-group image-preview [TO HERE]--> 
							
							<br />
							
							<!-- Drop Zone -->
							<div class="upload-drop-zone" id="drop-zone"> Or drag and drop files here </div>
							<br />
							<!-- Progress Bar -->
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"> <span class="sr-only">10% Complete</span> </div>
							</div>
							<br />

						</div>
					</div>
				</div>       
			</div>
		</div>';
	}

	function get_input_file_form2($image_input_name = "image-file", $media_id='-1', $prefix='user_ava', $id='')
	{
		return "
		<div class='button btn btn-light my_image_upload' style='padding:10px; height:110px; margin:3px 3px 3px 0; float:left;' image_id='".$id."'  prefix='$prefix'>
			<div class='pictogramm ' id='$prefix$id' style='position:relative; display:inline-block; height:90px; overflow:hidden;'>".
				_get_media($media_id, 90).
			"</div>
		</div>
		<input type='hidden' id='".$prefix."_media_id$id' name='$prefix$id' value='$media_id'/>";
	}

	function get_input_file_form3($image_input_name = "image-file", $media_id='-1', $prefix='user_ava', $id='')
	{
		$xt 		= _get_media_title( $media_id );
		return "
		<div class='button btn btn-secondary my_image_upload2' style='padding:10px; margin:3px 3px 3px 0; float:left;' image_id='".$id."'  prefix='$prefix'>
			<div class='pictogramm ' id='$prefix$id' style='position:relative; display:inline-block; overflow:hidden;'>
				<p>
					<small>".
						$xt[0].
					"</small>
				</p>			
				<span class='fi fi-" .$xt[1]. " fi-size-xs'>
					<span class='fi-content'>" .$xt[1]. "</span>
				</span>
			</div>
		</div>
		<input type='hidden' id='".$prefix."_media_id$id' name='$prefix$id' value='$media_id'/>";
	}



	function get_input_file_form4($image_input_name = "image-file")
	{
		return '
			<!-- bootstrap-imageupload. -->
			<div class="imageupload panel panel-default">
				<div class="panel-heading clearfix">
					<h3 class="panel-title pull-left">' . __("Upload Image", PLANDING) . '</h3>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default active">' . __("File", PLANDING) . '</button>
						<button type="button" class="btn btn-default">URL</button>
					</div>
				</div>
				<div class="file-tab panel-body">
					<label class="btn btn-default btn-file btn-lg">
						<span>' . __("Browse", PLANDING).'</span>
						<!-- The file is stored here. -->
						<input type="file" name="' . $image_input_name . '">
					</label>
					<button type="button" class="btn btn-default">' . __("Remove") . '</button>
					<button type="button" class="btn btn-default">' . __("Insert", PLANDING) . '</button>
				</div>
				<div class="url-tab panel-body">
					<div class="input-group">
						<input type="text" class="form-control hasclear" placeholder="Image URL">
						<div class="input-group-btn">
							<button type="button" class="btn btn-default">' . __("Submit", PLANDING).'</button>
						</div>
					</div>
					<button type="button" class="btn btn-default ">' . __("Remove").'</button>
					<!-- The URL is stored here. -->
					<input type="hidden" name="image-url">
				</div>
				<div class="panel-heading clearfix">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default">' .__( "Submit", PLANDING ) . '</button>
					</div>
				</div>
			</div>';
	}


	function get_input_file_form5($image_input_name = "image-file", $media_id='-1', $prefix='user_ava', $id='')
	{
		return "
		<div class='button btn btn-light my_image_upload' style='padding:0px; height:35px; wdth:100%; margin:0; float:left;' image_id='".$id."'  prefix='$prefix' width='100%' height='35'>
			<div class='pictogramm ' id='$prefix$id' style='position:relative; display:inline-block; height:35px; wdth:100%; overflow:hidden;'>".
				_get_media($media_id, 90).
			"</div>
		</div>
		<input type='hidden' id='".$prefix."_media_id$id' name='".$prefix."[".$id."]' value='$media_id'/>";
	}

	function _get_media_title($media_id)
	{
		$src	= wp_get_attachment_url( $media_id );
		$ext	= wp_check_filetype($src)['ext'];
		return 	$src ? [wp_basename( $src ), $ext] : ['', ""];
	}
	function _get_media($media_id, $size=300)
	{
		$src	= $size == "full" ?  wp_get_attachment_image_src($media_id, $size): wp_get_attachment_image_src($media_id, array($size, $size));
		if($src)
		{
			return "<img style='height:auto; width:".$size."px;' src='".$src[0]."'/>";
		}
		else
		{
			return "<img style='opacity:1; height:".$size."px; width:auto;' src='"._get_default( $file_url )."'/>";
		}
	}
	function _get_default($file_url = "")
	{
		return MCOURSES_URLPATH."assets/img/empty.png";
	}
}